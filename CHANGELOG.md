## 1.1.0 (2022-12-12)

THIS IS A MAJOR REWRITE (IF BELOW VERSION 1.0.0)! PLEASE REMOVE ALL PREVIOUS ACCESSORIES (ZONES) CREATED BY THIS PLUGIN FROM HOMEKIT BEFORE INSTALLING! ALSO, PLEASE UPDATE PLUGIN CONFIG AS THAT HAS CHANGED AS WELL.

### Notable Changes

* Added support for Homebridge plugin settings GUI

## 1.0.0 (2022-12-12)

THIS IS A MAJOR REWRITE! PLEASE REMOVE ALL PREVIOUS ACCESSORIES (ZONES) CREATED BY THIS PLUGIN FROM HOMEKIT BEFORE INSTALLING! ALSO, PLEASE UPDATE PLUGIN CONFIG AS THAT HAS CHANGED AS WELL.

### Notable Changes

* Zones now appear as audio receivers
* You can now change source for zones

### Bug Fixes

* MUCH faster response times, does not slow down HomeKit now

## 0.1.0 (2020-04-13)

### Notable Changes

* Initial release