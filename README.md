# Homebridge MCA-66 Plugin

Homebridge plugin for controlling HTD MCA-66 whole home audio controller. Currently, this only allows toggling zones on and off and changing sources from within the Home app. I plan on updating to add volume control as well.

## Installation

Requires `homebridge` and `net`

`npm -g install homebridge-htd-mca66`

## Configuration

1. Find the IP and port number of your HTD GW-SL1 Gateway and enter it in the configuration.
2. Add your zone names in the configuration.
3. Add your input names in the configuration.
4. Save the config, restart Homebridge.
5. Manually add new audio receivers (zones) by going into HomeKit, selecting Add Accessory and then selecting More options.
6. Enjoy!

### Sample config JSON

```json
{
    "name": "HTD",
    "host": "10.0.1.2",
    "port": "10006",
    "zones": [
        {
            "zone_1": "Living Room Speakers",
            "zone_2": "Gym Speakers",
            "zone_3": "Lake Porch Speakers",
            "zone_4": "Master Bedroom Speakers",
            "zone_5": "Master Bathroom Speakers",
            "zone_6": "Main Porch Speakers"
        }
    ],
    "inputs": [
        {
            "source_1": "AirPlay",
            "source_2": "Source Two",
            "source_3": "Source Three",
            "source_4": "Source Four",
            "source_5": "Source Five",
            "source_6": "Source Six"
        }
    ],
    "platform": "HTD"
}
```